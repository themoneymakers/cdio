function instrumentObjectMap = getDateMap(date, database)
% getValueDate - takes date and returns instrumentObject Map with ricCodes
% as keys
%
% Inputs:
%   date - date in form '20100110' (10 january 2010)
%
% Outputs:
%   instrumentObjectMap - Returns an map of objects
%
% >> getDateMap('20100112', usdIrsDataBase)
% 
% ans = 
% 
%   containers.Map handle
%   Package: containers
% 
%   Properties:
%         Count: 22
%       KeyType: 'char'
%     ValueType: 'any'
% 
%   Methods, Events, Superclasses
%
% Other m-files required: none
% MAT-files required: usdDatabase
% Other files required: none
%
% Author: Emil Karlsson
% November 2014; Last revision: 5-Nov-2014
%
%------------- BEGIN CODE --------------
if isKey(database,date)
  instrumentObjectMap = database(date);
else
    error('No such date in database')
end
end