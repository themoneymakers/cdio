function instrumentObject = getValueDate(date, ricCode, database)
% getValueDate - takes date and ricCode and returns instrumentObject
%
% Inputs:
%   date - date in form '20100110' (10 january 2010)
%   ricCode - ricCode of instrument
%
% Outputs:
%   instrumentObject - Returns an object of the date and ricCode
%
% >> getValueDate('20100112', 'USD4X7F=', usdIrsDataBase)
% 
% ans = 
% 
%   ForwardRateAgreement
% 
%   Properties:
%               yield: 734150
%        maturityDate: 122.3740
%        monthToStart: '4'
%     monthToDelivery: '7'
%             ricCode: 'USD4X7F='
%           issueDate: 0.3740
%            currency: 'USD'
%        nominalValue: 100
% 
%   Methods, Superclasses
% Other m-files required: none
% MAT-files required: usdDatabase
% Other files required: none
%
% Author: Emil Karlsson
% November 2014; Last revision: 5-Nov-2014
%
%------------- BEGIN CODE --------------
  ricCodeOnDate = database(date);
  instrumentObject = ricCodeOnDate(ricCode);
end