function [instrumentArray] = instrumentSort(instrumentArray, POLICY)
% instrumentSort - Unstable sorting of instruments
% Sorts instruments with respect to one of its data members, with
% selection sort. Warning, non-stable sort, so cannot be used repeativly
% to sort wrt two or more of the data members.
%
% Only sorting wrt maturity is implemented.
% No controls for data types implemented
%
%
% Inputs:
%    v - vector with instruments
%    POL - sorted by...
%
% Outputs:
%    vSort - sorted vector with instruments
%
% Examples: 
% >> instrumentSort([(a,4) , (b,3) , (b,2) , (c,3)]' , num)
%
% ans = 
%
% same vector sorted wrt to the number
%
%
% Other m-files required: Instrument
% MAT-files required: none
% Other files required: none
%
% Author: Pontus Ericsson
% October 2014; Last revision: 

%------------- BEGIN CODE --------------

% Selection-sort wrt POLICY.

N = length(instrumentArray);

if strcmp(POLICY , 'maturityDate')
    for i=1:N-1   
       for j=i+1:N
           if instrumentArray{j}.maturityDate < instrumentArray{i}.maturityDate
               temp_instrument = instrumentArray{i};
               instrumentArray{i} = instrumentArray{j};
               instrumentArray{j} = temp_instrument;
           end
       end
    end
end
%------------- END OF CODE --------------

