function [pastVolatilities] = calculateVolatility(pastReturns)
% calculateVolatility - Calculates past volatilities
% Uses past returns to calculate past volatilites using EWMA
%
% Inputs:
%    pastReturns [matrix] - past returns of instruments over a time period.
%    Each coloumn is an asset, and each row is a time (from newest
%    to oldest)
%
% Outputs:
%    pastVolatilities [matrix] - The past volatilites of each asset at each time
%
% Other m-files required: calculatePastReturns
% MAT-files required: none
% Other files required: none
%
% Author: Alexandra Stroh
% November 2014; Last revision: 08-11-2014

%------------- BEGIN CODE --------------

LAMBDA = 0.94;
[numberOfData numberOfAssets] = size(pastReturns);
pastVolatilities=zeros(numberOfData,numberOfAssets);

% ----------------------------------------
% Calculate the starting value for EWMA
for j = 1:numberOfData
    sumOfReturns(j,:) = (pastReturns(j,:) - mean(pastReturns)).^2;
end
pastVolatilities(numberOfData,:) = 1/(numberOfData-1)*sum(sumOfReturns);

% -----------------------------------------
% Calculate matrix of volatilites with EWMA

for i = numberOfData-1:-1:1
    pastVolatilities(i,:) = LAMBDA*pastVolatilities(i+1,:)+(1-LAMBDA)*pastReturns(i+1,:).^2;
end

%-----------------------------------------
% Converting the variance to volatility
% Converting daily volatility to yearly

pastVolatilities = sqrt(pastVolatilities*252);

end

%------------- END OF CODE --------------