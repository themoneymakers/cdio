classdef InterestRateSwap < InterestRateInstrument
    %INTERESTRATESWAP subclass of InterestRateIntstrument. Contains
    %information about an interest rate swap (fixed for IBOR)
    %   Properties:
    %   ricCode [string] = the contracts RIC-code, inherited from superclass
    %   issueDate [double] = the issue date of the contract, inherited from superclass
    %   yieldFix [double] = Yield of the fixed leg on issue date
    %   terminationDate [double] = Termination date of the contract
    %   fixDates [vector, double] = dates for payments on the fixed leg
    %   floatDates [vector, double] = dates for payment of the floating date
    %   maturityDate [double] = date for last payment on the floating leg,
    %   the instrument will not change in value after this date
    
    properties
        yieldFix;
        fixDates; % vector with dates to fixed pay 
        floatDates; % vector with dates to recieve floating payment
        
        spotDate;
        terminationDate;
        maturityDate;

        %timeToTermination;
        length; %maturityDate-issueDate
        dates; %business days to value the contract
        prices; %vector with prices 
        
    end
    
    methods
        function IRS = InterestRateSwap(r,i,yf,dateVector)
            %disp('Successfully called IRS-constructor')
            IRS@InterestRateInstrument(r,i);
            IRS.yieldFix = yf/100;
            IRS.spotDate=ceilToBusinessDay(ceilToBusinessDay(i+1,dateVector)+1,dateVector);

            
            
            if strcmp(r(end-1),'M')
                tempTimeToTermination = str2double(r(8:end-2));
            elseif strcmp(r(end-1),'Y')
                tempTimeToTermination = 12*str2double(r(8:end-2));
            else
                error('Undefined time unit for contract length.')
            end
            
            IRS.terminationDate = ceilToBusinessDay(addtodate(i,tempTimeToTermination,'month'),dateVector);
            %unRoundedTerminationDate = addtodate(i,tempTimeToTermination,'month');
            
            [fixedPeriodLength,floatingPeriodLength] = getInterestRateSwapConvention(r);
            
            fixN = floor(tempTimeToTermination/fixedPeriodLength);
            floatN = floor(tempTimeToTermination/floatingPeriodLength);

            tempFixDates = zeros(fixN,1);
            tempFloatDates = zeros(floatN,1);

            
            % Pretty messy for loops. If the total length of the contract isn't
            % dividible by the period length of payments, the FIRST payment
            % period will be the non standard one.
            for j = 0:fixN-1
                tempFixDates(fixN-j) = ceilToBusinessDay(addtodate(i,tempTimeToTermination-j*fixedPeriodLength,'month'),dateVector);
            end

            for j = 0:floatN-1
                tempFloatDates(floatN-j) = ceilToBusinessDay(addtodate(i,tempTimeToTermination-j*floatingPeriodLength,'month'),dateVector);
            end
            
            IRS.fixDates = tempFixDates;
            IRS.floatDates = tempFloatDates;
            
            IRS.maturityDate = tempFloatDates(end);
            
            %Sets length of contract
            IRS.length = tempTimeToTermination;
            numberOfBusinessDays = calculateNumberOfBusinessDayBetweenDates(i,IRS.terminationDate,dateVector);
            IRS.dates = zeros(numberOfBusinessDays,1);
            IRS.prices = zeros(numberOfBusinessDays,1);
       

            
        end
    end
    
end

