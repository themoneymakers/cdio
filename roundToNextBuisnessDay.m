function [buisnessDateNum] = roundToNextBuisnessDay(dateNum, buisnessDayListInput)
% roundToNextBuisnessDay - Rounds datenum object to next buisness day in
% list
%
% Inputs:
%    dateNum - datenum object
%
% Outputs:
%    buisnessDateNum - datenum object(next buisness day)
%
% Examples: 
% >> roundToNextBuisnessDay(734448, buisnessDayList)
% 
% ans =
% 
%       734450
%
% Other m-files required: none
% MAT-files required: buisnessDayList.mat
% Other files required: none
%
% Author: Emil Karlsson
% November 2014; Last revision: 10-Nov-2014

%------------- BEGIN CODE --------------
if dateNum > 735902 || dateNum < 726835 
  error('Date is outside of our range');
end

iterNumber=1;
while buisnessDayListInput(iterNumber)<dateNum
  iterNumber = iterNumber + 1;
end

buisnessDateNum = buisnessDayListInput(iterNumber);
end
%------------- END OF CODE --------------

