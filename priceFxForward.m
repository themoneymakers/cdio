function priceOfFxForward = priceFxForward(elementFxForward, valuationNumber, businessDayList, forwardMatrixDomestic, forwardMatrixForeign, strikePrice, valuationDate)

numberOfInstruments = length(elementFxForward);
priceOfFxForward = zeros(numberOfInstruments,1);

for j = 1:numberOfInstruments
   issueDate = elementFxForward{j}.issueDate;
   maturityDate = elementFxForward{j}.maturityDate;
   spotDate = elementFxForward{j}.spotDate;
   
   issueNumber = find(businessDayList == issueDate);
    
   discountRateDomesticK = findDiscountRate(issueNumber, spotDate, forwardMatrixDomestic,issueDate, maturityDate);
   discountRateForeignK = findDiscountRate(issueNumber, spotDate, forwardMatrixForeign, issueDate, maturityDate);
   strikePriceK = strikePrice(issueDate)*exp(discountRateDomesticK-discountRateForeignK);
   
   discountRateDomestic = findDiscountRate(valuationNumber, spotDate, forwardMatrixDomestic, valuationDate, maturityDate);
   discountRateForeign = findDiscountRate(valuationNumber, spotDate, forwardMatrixForeign, valuationDate, maturityDate);
   
   priceOfFxForward(j) = strikePrice(valuationDate)*exp(-discountRateForeign)-strikePriceK*exp(-discountRateDomestic);
   
end

end

function discountRate = findDiscountRate(valuationNumber, spotDate, forwardMatrix, valuationDate, maturityDate)
% Finds discount rate from the discountMatrix
%
% Inputs   
%   valuationNumber - the number of the row corresponding to the date that
%   is valued
%   discountMatrix
%   valuationDate
%   issueDate
%   maturityDate
%   terminationDate
%
% Outputs
%   discountRate - the two discount factors needed for calculating the FRA
%
%------------- BEGIN CODE --------------

if (valuationDate < spotDate)
    startValue = spotDate-valuationDate;
else
    startValue = 1;
end
    coloumnNumber = maturityDate - valuationDate;

    discountRate = sum(forwardMatrix(valuationNumber, startValue:coloumnNumber))/100;
end