classdef ForwardRateAgreement < InterestRateInstrument
%FORWARDRATEAGREEMENT subclass of InterestRateIntstrument
%   Detailed explanation goes here
%Example of call:
%ForwardRateAgreement('EUR3X6F=', now, 'EUR', 5)

  properties
    yield;
    spotDate;
    maturityDate;
    terminationDate;
    floatingRate; % floating rate to be recieved at maturity as a number of months
    length; %terminationDate-issueDate
    dates; % business days to value the contract
    prices; % vector with prices 

  end

  methods
    function FRA = ForwardRateAgreement(r,i,y,dateVector)
        FRA@InterestRateInstrument(r,i);
        FRA.spotDate=ceilToBusinessDay(ceilToBusinessDay(i+1,dateVector)+1,dateVector);
        FRA.yield = y/100; %Transform from percentage to 
        
        ricCode = strrep(r, '=', '');
        lengthString = ricCode(4:end);
        %Fixes that a Swedish FRA don't have any ending character
        if isstrprop(lengthString(end), 'alpha')
          lengthString = ricCode(4:end-1);
        end

        if isstrprop(lengthString(2), 'alpha')
          FRA.maturityDate = ceilToBusinessDay(addtodate(i,str2double(lengthString(1)),'month'),dateVector);
          tempTerminationDate = ceilToBusinessDay(addtodate(i,str2double(lengthString(3:end)),'month'),dateVector);
          FRA.terminationDate = tempTerminationDate;
          %fprintf(num2str(str2double(lengthString(3:end)) - str2double(lengthString(1))))
          FRA.floatingRate  = str2double(lengthString(3:end)) - str2double(lengthString(1));
        elseif isstrprop(lengthString(3), 'alpha')
          FRA.maturityDate = ceilToBusinessDay(addtodate(i,str2double(lengthString(1:2)),'month'),dateVector); 
          tempTerminationDate = ceilToBusinessDay(addtodate(i,str2double(lengthString(4:end)),'month'),dateVector);
          FRA.terminationDate = tempTerminationDate;
          FRA.floatingRate  = str2double(lengthString(4:end)) - str2double(lengthString(1:2));
        else
          error('Cannot parse RicCode for FRA correctly.');
        end
        
        %Sets length of contract
        FRA.length = i-FRA.maturityDate;
        numberOfBusinessDays = calculateNumberOfBusinessDayBetweenDates(i,tempTerminationDate,dateVector);
        FRA.dates = zeros(numberOfBusinessDays,1);
        FRA.prices = zeros(numberOfBusinessDays,1);
       

    end
  end
end

