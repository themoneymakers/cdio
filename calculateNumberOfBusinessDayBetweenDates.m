function numberOfDayCount = calculateNumberOfBusinessDayBetweenDates(dateOne, dateTwo, dateVector)
%calculateNumberOfBusinessDayBetweenDates - Calculate the number of
%buisnessDays between two dates based on dateVector (does not count the
%last date)
%
% Inputs:
%    dateOne - datenum of first date
%    dateTwo - datenum of second date
%    dateVector - vector of business days
%
% Outputs:
%    numberOfDay - number of days between two business days
%
% Examples: 
% >> calculateNumberOfBusinessDayBetweenDates(726835, 735902, businessDayList)
% 
% ans =
% 
%         6216
%
% Other m-files required: none
% MAT-files required: businessDayList.mat
% Other files required: none
%
% Author: Emil Karlsson
% November 2014; Last revision: -

maxDate = max(dateVector);
numberOfDayCount = 0;

%Implementation of C++-style swap.
swap=@(varargin)varargin{nargin:-1:1};
if dateOne > dateTwo
  [dateOne dateTwo] = swap(dateOne, dateTwo);
elseif dateOne == dateTwo
  error('The same start-/enddate')
end

dateOne = ceilToBusinessDay(dateOne, dateVector);
dateTwo = ceilToBusinessDay(dateTwo, dateVector);
%Cuts at last dateVector value
dateTwo = min(maxDate+1, dateTwo);

dateVectorIterator = 1;
dateVectorLength = length(dateVector);
%Possible to implement a version that avoids looping entire dateVector
while dateVectorIterator < dateVectorLength && dateVector(dateVectorIterator) < dateTwo
  if (dateOne <= dateVector(dateVectorIterator))
    numberOfDayCount = numberOfDayCount + 1;
  end
  dateVectorIterator = dateVectorIterator + 1;
end
%Converts to integer
numberOfDayCount=round(numberOfDayCount);
end