function [gradientVector, hessianMatrix] = calculateGradientIrs(interestRateSwap, riskFactorEigenMatrix, evaluationDate, businessDayList, forwardMatrix)
%>> [a,b]=calculateGradientIrs(testContract, riskFactorEigenMatrix, 731920, businessDayList, forwardMatrixUSD)
%r(0)T_0=0 just nu i formel?
%Tidsperioden som kontinuerlig tid eller antalet arbetsdagar?
%Vad �r ck?
%�r beroende av spotMatrixUSD(eller forward utr�kning n�r det kr�vs), firstRiskFactorEigenvectors

%funktion som tar en viss dag i datenum samt antal dagar i framtiden och returnerar det v�rdet i r�ntekurvan

numberOfPayments = length(interestRateSwap.floatDates);
numberOfRiskFactors = size(riskFactorEigenMatrix,2);
c = ones(numberOfPayments,1);

lengthToSpotDate = max(0,interestRateSwap.spotDate-evaluationDate);
startDate = max(interestRateSwap.spotDate, evaluationDate);

if (lengthToSpotDate == 0)
  interestRateTZero = 0; %r(T_0)T_0
  eigenVectorTZero = zeros(1,numberOfRiskFactors);
else
  interestRateTZero = forwardMatrix(1000,lengthToSpotDate);
  eigenVectorTZero = riskFactorEigenMatrix(lengthToSpotDate, :);
end

hessianMatrix = zeros(numberOfRiskFactors);
gradientVector = zeros(numberOfRiskFactors,1);
for k=1:numberOfPayments
  if(interestRateSwap.floatDates(k)>startDate)
    lengthK = calculateNumberOfBusinessDayBetweenDates(interestRateSwap.floatDates(k), startDate, businessDayList);
    
    hessianMatrix = hessianMatrix-(riskFactorEigenMatrix(lengthK, :)*lengthK-eigenVectorTZero*lengthToSpotDate)'*(riskFactorEigenMatrix(lengthK, :)*lengthK-eigenVectorTZero*lengthToSpotDate)*c(k)*exp(interestRateTZero-forwardMatrix(1000,lengthK));

    gradientVector = gradientVector-(riskFactorEigenMatrix(lengthK, :)*lengthK-eigenVectorTZero*lengthToSpotDate)'*c(k)*exp(interestRateTZero-forwardMatrix(1000,lengthK));
  end
end

end