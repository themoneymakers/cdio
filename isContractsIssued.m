function isissued = isContractsIssued( date,database )
%ISCONTRACTSISSUED Checks if the database contains a date.
%   Returns true if any contracs are issued, false otherwise.
    if isKey(database,date)
        isissued = true;
    else
        isissued = false;
    end
end

