function price = evaluateGeneralCase(priceScalar, gradVector, hessianMatrix) %Values from risk factors as input
% evaluateGeneralCasee - Calculate value based on risk changes
%
% Inputs:
%    priceScalar - P(0) 
%    gradVector - gradient of instrument
%    hessianMatrix - Hessian of instrument
%
% Outputs:
%    price - Price based on changes in risk factors
%
% Examples: 
% >> evaluateGeneralCase(10, [1 2 3 4 5]', [1 2 3 4 5; 1 2 3 4 5; 1 2 3 4 5; 1 2 3 4 5; 1 2 3 4 5])
%
%
% Other m-files required: none
% MAT-files required: none
% Other files required: none
%
% Author: Evelina o Emil
% November 2014; Last revision: 06-Nov-2014
%------------- BEGIN CODE --------------

%Assumptions on risk factors, estimated from data
mu = zeros(5,1);
Sigma = eye(5);

%Calculated from day to day change in the risk factors
randomVector = mvnrnd(mu, Sigma)';

%Price for a single asset, based on change in risk factors
price = priceScalar + gradVector'*randomVector + 1/2*(randomVector'*hessianMatrix*randomVector);

%------------- END OF CODE --------------

