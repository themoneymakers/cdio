function valueFxForward=valuateFxForward(elementFxForward, spotRates, valuationDate, maturityDate)
%Strike price (K) = F_0, dvs priset d� kontraktet ing�s
%priceFxForward �r f�r dagen den v�rderas

% log(1+r)-1 (?) f�r att f� nollkup. r�nta - �ven om det �r f�r en riskfri
% r�nta?


%spotRate - vektor med  tv� element, v�xelkurs f�r valuationDate samt issueDate 

strikePriceFxForward=spotRate(1)*exp((riskFreeRateForeign-riskFreeRateDomestic)*maturity);
priceFxForward=spotRate(2)*exp((riskFreeRateForeign-riskFreeRateDomestic)*timeToMaturity);

valueFxForward=(priceFxForward-strikePriceFxForward)*exp(riskFreeRateDomestic*timeToMaturity);

end

%function calcualatePriceFxForward=calculatePriceFxForward(spotPriceCurrency,riskFreeRateDomestic,riskFreeRateForeign,timeToMaturity)

%Borde skapa pris matris ? och sen i valuate anropa och f� en matris och
%anv�nda plats noll som K


%spotPriceCurrencey                 % Input from yield curve
%riskFreeRateDomestic               % Input from yield curve
%riskFreeRateForeign                % Input from yield curve (T-t)
%timeToMaturity                     % Input from yield curve
%spotPriceCurrency;                  %


%priceFxForward=spotPriceCurrency*exp((riskFreeRateForeign-riskFreeRateDomestic)*timeToMaturity);

%end

%n;                              % n=width(data matrix)(antal kolumner=kontrakt(?)) 
%m;                              % m=length(data matrix)- antal rader = datum 

%for i=1;n
%    for j=1;m
    
%priceFxForward(j,i)=spotPriceCurrency(j,i)*exp((riskFreeRateForeign(j,i)-riskFreeRateDomestic(j,i))*timeToMaturity(j,i));

%    end
%end