function [activeContracts] = getActiveContractForPortfolio(portfolioArray, date)
activeContracts={};

for i = 1:length(portfolioArray)
    
    datestr(portfolioArray{i}.maturityDate)
    datestr(portfolioArray{i}.issueDate)
    date
    portfolioArray{i}.maturityDate > date
    portfolioArray{i}.issueDate <= date
    if (portfolioArray{i}.maturityDate > date) && (portfolioArray{i}.issueDate <= date)
        activeContracts{end+1,1}=portfolioArray{i};
    end
end
end