function [priceOfIrs time] = priceIrsRiskMetrics(elementIrs, LIBOR3M, spotRateFloat, spotRateFix, valuationDate)
% Price IRS at valuationDate
% Recieves floating rate and pays fixed rate
%
% Inputs   
%   elementIrs - element containing information about IRS
%       elementIrs.issueDate (scalar)
%       elementIrs.fixDates (vector)
%       elementIrs.floatDates (vector)
%       elementIrs.yieldFix (scalar)
%       elementIrs.nominalValue (scalar)
%   LIBOR3M (Container) - simple three months libor rates for all dates
%   spotRate [vector] - vector containing fix and floating spotRates
%                      [floatSpotRate fixSpotRate fixSpotRate ... ]
%   valuationDate [double] - the date the IRS should be valued
%
% Outputs
%   priceOfIrs [vector] - the present value of different cashflows
%   time [vector] - the time for the cash flows

% Example: 
%     >> [price time] = priceIrsRiskMetrics(tempIrs{1},LIBOR3M, spotRate, 732836)
% 
%     price =
% 
%       100.8448   -2.7171   95.3297
% 
% 
%     time =
% 
%         0.2521    0.5014    1.0000
%
%------------- BEGIN CODE ----------------------------------------------

nominalValue = elementIrs.nominalValue;
yieldFix = elementIrs.yieldFix;
issueDate = elementIrs.issueDate;
fixDates = elementIrs.fixDates;
floatDates = elementIrs.floatDates;

if(valuationDate > max(fixDates(end),floatDates(end)))
    error('Valuation Date greater than Maturity Date')
end

% ----------------------------------------------------------------------
% Price of floating leg

[timeCountFloat timeForNextFloatingPayment timeForLastFixing elementSpotRateFloat] = calculateTimeCountFloat(valuationDate, floatDates);
time(1) = (timeForNextFloatingPayment-valuationDate)/365;
discountFactorFloat= exp(-(spotRateFloat(elementSpotRateFloat)*time(1)));

if (timeForLastFixing == 0)
    timeForLastFixing = issueDate;
end

yieldFloat = LIBOR3M(timeForLastFixing);

priceOfIrs(1) = nominalValue*(1+yieldFloat*timeCountFloat)*discountFactorFloat;

% -------------------------------------------
% Price of fixed leg
[timeCountFix timeForFixPayments numberOfFixPayments] = calculateTimeCountFix(valuationDate, fixDates);

for k = 1:numberOfFixPayments
    time(k+1) = (timeForFixPayments(k)-valuationDate)/365;
    discountFactorFix = exp(-(spotRateFix(k)*time(k+1)));
    priceOfIrs(k+1) = -nominalValue*yieldFix*timeCountFix(k)*discountFactorFix;
end

priceOfIrs(numberOfFixPayments+1) = -(priceOfIrs(numberOfFixPayments+1) + nominalValue*discountFactorFix);

end

%------------- END CODE ----------------------------------------------

function [timeCountFloat timeForNextFloatingPayment timeForLastFixing elementSpotRateFloat] = calculateTimeCountFloat(valuationDate, paymentDates)
% Calculates needed date and time for floating leg
%
% Inputs
%   valuationDate
%   paymentDates [vector] - containing dates for all floating payments
%
% Outputs
%   timeCountFloat - Proper time count for next floating payment
%   timeForNextFloatingPayment - The date of the next floating payment
%   timeForLastFixing - The date of the fixing of the floating rate 

%------------- BEGIN CODE -----------------------------------------------

for i = 1:length(paymentDates)
    timeToAllPayments(i) = (paymentDates(i) - valuationDate)/360;
end

timeCountFloat = timeToAllPayments(timeToAllPayments>0);
timeForAllFloatingPayment = paymentDates(timeToAllPayments>0);
elementSpotRateFloat = length(timeToAllPayments) - length(timeForAllFloatingPayment)+1;
timeCountFloat = timeCountFloat(1);
timeForNextFloatingPayment = timeForAllFloatingPayment(1);

if (length(paymentDates) == length(timeForAllFloatingPayment))
    timeForLastFixing = 0;
else
    timeForLastFixing = paymentDates(timeToAllPayments<0);
    timeForLastFixing = timeForLastFixing(end);
end
end

%------------- END CODE -------------------------------------------------

function [timeCountFix timeForFixPayments numberOfFixPayments] = calculateTimeCountFix(valuationDate, paymentDates)
% Calculates dates and times for fixed leg
%
% Inputs
%   valuationDate
%   paymentDates [vector] -  containing dates for all fixed payments
%
% Outputs
%   timeCountFix - proper time count for all fixed payments
%   timeForFixPayments - the dates for all future payments 
%   numberOfFixPayments - total number of fixed payments

%------------- BEGIN CODE -----------------------------------------------

numberOfPayments = length(paymentDates);

for i = 1:numberOfPayments
    testTime(i) = paymentDates(i)-valuationDate;
end

timeForFixPayments = paymentDates(testTime>0);
numberOfFixPayments=length(timeForFixPayments);

dateVectorValuation = datevec(valuationDate);
if dateVectorValuation(3) == 31
    dateVectorValuation(3) = 30;
end

for j=1:numberOfFixPayments
    dateVectorFix = datevec(timeForFixPayments(j));
    if (j == 1)
        dateVectorValuation = datevec(valuationDate);
    else
        dateVectorValuation = datevec(timeForFixPayments(j-1));
    end
    if dateVectorFix(3) == 31
        dateVectorFix(3) = 30;
    end
    if dateVectorValuation(3) == 31
    dateVectorValuation(3) = 30;
    end
    timeCountFix(j) = 1/360*(360*(dateVectorFix(1)-dateVectorValuation(1))+30*(dateVectorFix(2)-dateVectorValuation(2))+(dateVectorFix(3)-dateVectorValuation(3)));
end

end
