function [priceOfFra time] = priceFraRiskMetrics(elementFra, spotRate, valuationDate)
% Price FRA at time valuationDate
% Recieves fixed rate "yieldFRA" between two times
%
% Inputs   
%   elementIrs - elements containing information about IRS
%       elementFra.issueDate (scalar)
%       elementFra.terminationDate (scalar)
%       elementFra.maturityDate (scalar)
%       elementFra.yield (scalar)
%       elementFra.nominalValue (scalar)
%   valuationNumber [double] - the number corresponding to right date in discountMatrix
%   discountMatrix [matrix] - matrix with all discount rates
%   valuationDate [double] - the date the FRA should be valued
%
% Outputs
%   priceFRA [vector] - the price/value of each FRA contract (elementFra) at valuationDate
%

%------------- BEGIN CODE --------------

% Sets values
nominalValue = elementFra.nominalValue;
yield = elementFra.yield;
maturityDate = elementFra.maturityDate;
terminationDate = elementFra.terminationDate;
issueDate = elementFra.issueDate;
 
% Set proper Tc with appropriate time convention
timeCountTc = timeCount(maturityDate, terminationDate);

if (valuationDate > maturityDate)
    error('valuation date greater than maturity date. no value')
end

time = (maturityDate - valuationDate)/365;
timeT2 = (terminationDate - valuationDate)/365;
        
discountFactorT1 = exp(-(spotRate(1)*time));
discountFactorT2 = exp(-(spotRate(2)*timeT2));

priceOfFra = nominalValue*(1+yield/100*timeCountTc)*discountFactorT2-nominalValue*discountFactorT1;
 
end


%------------- END CODE --------------

function timeCount = timeCount(firstDate, secondDate)
% Calculated the time to payment with right dayconventions
%
% Inputs   
%   firstDate
%   secondDate
%
% Outputs
%   timeCount
%
%------------- BEGIN CODE --------------
timeCount = (secondDate-firstDate)/360;

end

%------------- END CODE --------------

