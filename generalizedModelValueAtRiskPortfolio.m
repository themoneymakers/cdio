function valueAtRisk = generalizedModelValueAtRiskPortfolio(portfolio)


matrixG%= %for all contract in portfolio calculate gradient (g_1 ... g_n)

%Simple
g%=%sum(i\in 1:n) holding*gradient_asset_i
H%=sum(i\in 1:n) holding*Hessian_i

valueAtRisk = simulateGeneralMethodDistribution(gradient, Hessian, meanVector, covarianceMatrix, numberOfIterations)

end