classdef ForeignExchangeForward
    %FOREIGNEXCHANGEFORWARD Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        ricCode; % Made up ric-code e.g. 'XYZABXi='.
        issueDate;
        spotDate;
        maturityDate;
        domesticCurrency;
        foreignCurrency;
        short;
        dates;
        prices;
        
    end
    
    methods
        function FXF = ForeignExchangeForward(rc, id, dateVector)
            FXF.ricCode = rc;
            FXF.issueDate = id;
            %FXF.spotDate = ceilToBusinessDay(addtodate(id,2,'day'),dateVector);
            FXF.spotDate=ceilToBusinessDay(ceilToBusinessDay(id+1,dateVector)+1,dateVector);
            FXF.maturityDate = ceilToBusinessDay(addtodate(id,str2double(rc(7:end-1)),'month'),dateVector);
            FXF.domesticCurrency = rc(4:6);
            FXF.foreignCurrency = rc(1:3);
            FXF.short = false;
            
            numberOfBusinessDays = calculateNumberOfBusinessDayBetweenDates(id,FXF.maturityDate,dateVector);
            FXF.dates=zeros(numberOfBusinessDays,1); % Allokerar bara plats
            FXF.prices=zeros(numberOfBusinessDays,1); % Allokerar bara plats
        end
    end
    
end

