%loadAll
%valuationDate=datenum(2000,10,16);
%temp=usdFraDataBase(valuationDate)
%elementFra=temp{20}
%valuationNumber=find(businessDayList==valuationDate);
%caculateRiskMetricsCashFlows(elementFra, valuationNumber, valuationDate, forwardMatrixUSD)

function [riskMetricsCfFra riskMetricsCfIrs riskMetricsCfFx]=caculateRiskMetricsCashFlows(element, LIBOR3M, valuationNumber, valuationDate, forwardMatrixUSD)

discountMatrix=calculateDiscountMatrix(forwardMatrixUSD);
spotRateMatrix=calculateSpotRateMatrix(forwardMatrixUSD);
%riskMetricsCfFra=calculateRiskMetricsFra(element,discountMatrix, valuationNumber, valuationDate, spotRateMatrix);
riskMetricsCfIrs=calculateRiskMetricsIrs(element,LIBOR3M,valuationNumber, valuationDate, spotRateMatrix);
riskMetricsCfFx=0;
riskMetricsCfFra=0;


end


%-------- OBS OBS OBS zeroYield=exp(spotRate)-1--------------------
function presentValueIrs=calculateRiskMetricsIrs(elementIrs,LIBOR3M,valuationNumber, valuationDate, spotRateMatrix)
%riskMetricsCashFlowIrs
% Price IRS at valuationDate
% Recieves floating rate and pays fixed rate
%
% Inputs   
%   elementIrs - element containing information about IRS
%       elementIrs.issueDate (scalar)
%       elementIrs.fixDates (vector)
%       elementIrs.floatDates (vector)
%       elementIrs.yieldFix (scalar)
%       elementIrs.nominalValue (scalar)
%       [priceOfIrs time] = priceIrsRiskMetrics(elementIrs, LIBOR3M, spotRateFloat, spotRateFix, valuationDate)

fixedDate=elementIrs.fixDates;
floatDates=elementIrs.floatDates;

for iFixDates=1:size(fixedDate)
    
    daysToCashFlow(iFixDates)=fixedDate(iFixDates)-valuationDate;
    timeToMaturityRmFixed(iFixDates:iFixDates+1)=calcTimeToMaturityRm(daysToCashFlow(iFixDates));
    
    if (timeToMaturityRmFixed(iFixDates)==0)
        zeroYieldsFixed(iFixDates)=0;
    else
        zeroYieldsFixed(iFixDates)=exp(spotRateMatrix(valuationNumber, ceil(365*timeToMaturityRmFixed(iFixDates)/12)))-1;
    end
    
    zeroYieldsFixed(iFixDates+1)=exp(spotRateMatrix(valuationNumber, ceil(365*timeToMaturityRmFixed(iFixDates+1)/12)))-1;
    interpolatedZeroYieldFix(iFixDates:iFixDates+1)=interpolate(daysToCashFlow(iFixDates),timeToMaturityRmFixed(iFixDates:iFixDates+1), zeroYieldsFixed(iFixDates:iFixDates+1)); 
    
    for jFloatDates=1:size(floatDates)
        daysToCashFlow(jFloatDates)=floatDates(iFixDates)-valuationDate;
        timeToMaturityRmFloat(jFloatDates:jFloatDates+1)=calcTimeToMaturityRm(daysToCashFlow(jFloatDates));
        
        if (timeToMaturityRmFloat(jFloatDates)==0)
            zeroYieldsFloat(jFloatDates)=0;
        else
            zeroYieldsFloat(jFloatDates)=exp(spotRateMatrix(valuationNumber, ceil(365*timeToMaturityRmFloat(jFloatDates)/12)))-1;
        end
    
    zeroYieldsFloat(jFloatDates+1)=exp(spotRateMatrix(valuationNumber, ceil(365*timeToMaturityRmFloat(jFloatDates+1)/12)))-1;
    interpolatedZeroYieldFloat(jFloatDates:jFloatDates+1)=interpolate(daysToCashFlow(jFloatDates),timeToMaturityRmFloat(jFloatDates:jFloatDates+1), zeroYieldsFloat(jFloatDates:jFloatDates+1)); 
    
    end
        
%interpolate(timeToMaturityAcualCF,timeToMaturityRM, variableRM)    
    
end

presentValueIrs= priceIrsRiskMetrics(elementIrs, LIBOR3M, interpolatedZeroYieldFloat, interpolatedZeroYieldFix, valuationDate)

end


function riskMetricsCashFlowFra=calculateRiskMetricsFra(element,discountMatrix, valuationNumber, valuationDate, spotRateMatrix)



%zeroYield=exp(spotRate-1);

daysToCashFlow(1)=element.maturityDate-valuationDate;
daysToCashFlow(2)=element.terminationDate-valuationDate;

%businessDaysToCashFlow(1)=calculateNumberOfBusinessDayBetweenDates(valuationDate,elementFra.maturityDate, dateVector);
%businessDaysToCashFlow(2)=calculateNumberOfBusinessDayBetweenDates(valuationDate,elementFra.terminationDate, dateVector);

timeToMaturityRma=calcTimeToMaturityRm(daysToCashFlow(1));
timeToMaturityRmb=calcTimeToMaturityRm(daysToCashFlow(2));
timeToMaturityRm=[timeToMaturityRma(1) timeToMaturityRma(2) timeToMaturityRmb(1) timeToMaturityRmb(2)];
%------- Gets the yields for zero coupon bonds for RiskMetrics vertics----

for i=1:4
    if (timeToMaturityRm(i)==0)
        zeroYields(i)=0;
    else
        zeroYields(i)=exp(spotRateMatrix(valuationNumber, ceil(365*timeToMaturityRm(i)/12)))-1;
        %zeroYields(i)=temp(1);                                              
    end
end

%----Step 1 RiskMetrics: Calculate interpolated yields for maturity and termination days for FRA-----
interpolatedYield(1)=interpolate(daysToCashFlow(1),ceil(365*timeToMaturityRm(1:2)/12), zeroYields(1:2));  %�ndra s� det �r faktisk timeToMaturity och anv. businessdays endast n�r v�rden ska h�mtas fr�n matris
interpolatedYield(2)=interpolate(daysToCashFlow(2),ceil(365*timeToMaturityRm(3:4)/12), zeroYields(3:4));

%----Step 2 RiskMetrics: Calculate PV for FRA cashflows-----------------
%presentValueAcualCashFlowFRA=priceFra(element, valuationNumber, interpolatedYield, valuationDate)

presentValueAcualCashFlowFRA=priceFraRiskMetrics(element, interpolatedYield, valuationDate);

%----Step 3 RiskMetrics: Calculate interpolated volatility FRA-----------------
%valuateZeroCouponBonds(valuationNumber, discountMatrix, valuationDate, time)
presentValueBondVertic1 = valuateZeroCouponBonds(valuationNumber,discountMatrix,valuationDate(1),timeToMaturityRm(1)/12);
presentValueBondVertic2= valuateZeroCouponBonds(valuationNumber,discountMatrix,valuationDate(1),timeToMaturityRm(2)/12);
%presentValueBond=[presentValueBondVertic1(1) presentValueBondVertic2(1)];

pastReturnsBond1 = calculatePastReturns(presentValueBondVertic1);
pastReturnsBond2 = calculatePastReturns(presentValueBondVertic2);

pastVolatilityVertic1 = calculateVolatility(pastReturnsBond1);
pastVolatilityVertic2 = calculateVolatility(pastReturnsBond2);

pastVolatility=[pastVolatilityVertic1(1) pastVolatilityVertic2(1)];
 
correlationVertics1_2 = calculateCorrelation(pastReturnsBond1, pastReturnsBond2);

interpolatedVolatility=interpolate(daysToCashFlow(1),ceil(365*timeToMaturityRm(1:2)/12), pastVolatility);

%----Step 4 RiskMetrics: Calculate allocation alpha & (1-alpha)-----------------

alpha=calculateAlphaRiskMetrics(pastVolatility, interpolatedVolatility, correlationVertics1_2);

riskMetricsCashFlowFra=calculateriskMetricsCf(alpha, presentValueAcualCashFlowFRA);

end


%----------step 4: calcualate allocation (alpha and (1-alpha) for RiskMetrics CF-------------
function alpha=calculateAlphaRiskMetrics(pastVolatility,interpolatedVolatility, pastCorrelationVetics1_2)

a=pastVolatility(1)^2+pastVolatility(2)^2-2*pastCorrelationVetics1_2*pastVolatility(1)*pastVolatility(2);
b=2*pastCorrelationVetics1_2*pastVolatility(1)*pastVolatility(2)-2*pastVolatility(2)^2;
c=pastVolatility(2)^2-interpolatedVolatility^2;

alphaSub=(-b-sqrt(b^2-4*a*c))/(2*a);
alphaAdd=(-b+sqrt(b^2-4*a*c))/(2*a);

if (0<=alphaSub<=1)
    alpha=alphaSub;
else
    alpha=alphaAdd;
end

end


%----------step 5: calcualate RiskMetrics CF-------------
function riskMetricsCf = calculateriskMetricsCf(alpha, actualCashFlow)

%H�r ska test g�ras m 5 villkor
    riskMetricsCf(1)=alpha*actualCashFlow;
    riskMetricsCf(2)=(1-alpha)*actualCashFlow;

end


%------------Help function: interpolate------------------------------------
%linear interpolation function, used to determaine RiskMetric's yields and volatilities---

function interpolatedValue=interpolate(timeToMaturityAcualCF,timeToMaturityRM, variableRM)

    if (timeToMaturityRM(1)==0)
        interpolatedValue=variableRM(2);        
    else
        interpolatedValue=variableRM(1)+(variableRM(2)-variableRM(1))*(timeToMaturityAcualCF-timeToMaturityRM(1))/(timeToMaturityRM(2)-timeToMaturityRM(1));
    end
end

%---------Help function: timeToMaturityRm----------------------------------
%decides which RiskMetrics vertics the actual cash flow occur inbetween. 

function timeToMaturityRm=calcTimeToMaturityRm(timeToMaturity)

riskMetricsMaturities=[0 1/12 3/12 1/2 1 2 3 4 5 7 9 10 15 20 30];
timeToMaturityRm=[0 0];

j=0;
i=1;
while j==0
    if(360*riskMetricsMaturities(i)<timeToMaturity && timeToMaturity<360*riskMetricsMaturities(i+1))
        timeToMaturityRm=[12*riskMetricsMaturities(i) 12*riskMetricsMaturities(i+1)];
        j=1;
    elseif (timeToMaturity==360*riskMetricsMaturities(i) && timeToMaturity ~=0)
        timeToMaturityRm=[0 12*riskMetricsMaturities(i)];
        j=1;
    elseif (timeToMaturityRm(1)==0 && timeToMaturityRm(2)==0  && i<15)
        i=i+1;
    elseif (timeToMaturity>360*riskMetricsMaturities(15))
        timeToMaturityRm=[0 12*riskMetricsMaturities(15)];
        j=1;
    elseif (timeToMaturity==0)
        timeToMaturityRm=[0 12*riskMetricsMaturities(2)];
        j=1;
    else
        error('error in calcTimeToMaturity')
    end
 end

end

