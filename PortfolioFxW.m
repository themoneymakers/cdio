% Portfolio containing FX forward. Updated every two weeks since 1999
Fx=usdFxfDataBase(datenum(1999,10,25));
contractsFx={Fx{1};Fx{2};Fx{13};Fx{14}}; %ska �ndras
activeContractsFx_EUR = {Fx{1};Fx{13}};
activeContractsFx_SEK = {Fx{2};Fx{14}};
startDate = datenum(1999,10,25);
endDate = datenum(2000,10,25);
SHORT_DIST_FX=0.77;
MID_DIST_FX=0.20;
LONG_DIST_FX=0.084;
k=4; %antal kontrakt i portf�ljen vid start
n=-1;
sumOfPriceFx_SEK = zeros(endDate-startDate+1,1);
sumOfPriceFx_EUR = zeros(endDate-startDate+1,1);
sumOfPortfolio = zeros(endDate-startDate+1,1);


for i=startDate:endDate
    valuationNumber = find(businessDayList == i);
    n=n+1;
      if n==14
        if isContractsIssued(i,usdFxfDataBase)
        [short,mid,long]=getPortfolioDistribution(contractsFx,i);
        [instr_short,instr_mid,instr_long]=splitContractByLength(usdFxfDataBase(i)); %ska �ndras
        
         if short<=SHORT_DIST_FX && not(isempty(instr_short))&& not(isempty(valuationNumber))
            tmp=randi(length(instr_short));
            for j=1:tmp
            temp=randi(length(instr_short));
            k=k+1;
            instr_short{temp}.short=round(rand(1)); %L�ng eller kort position slumpas.
            contractsFx{k,1} = instr_short{temp};
           if instr_short{temp}.foreignCurrency == 'SEK'
            activeContractsFx_SEK{end+1} = contractsFx{k,1};
           else
            activeContractsFx_EUR{end+1} = contractsFx{k,1};    
           end
            temp=0;
            end
            tmp=0;
         end
        if mid<=MID_DIST_FX && not(isempty(instr_mid))&& not(isempty(valuationNumber))
            tmp=randi(length(instr_mid));
            for j=1:tmp
            temp=randi(length(instr_mid));
            k=k+1;
            instr_mid{temp}.short=round(rand(1)); %L�ng eller kort position slumpas.
            contractsFx{k,1} = instr_mid{temp};
           if instr_mid{temp}.foreignCurrency == 'SEK'
            activeContractsFx_SEK{end+1} = contractsFx{k,1};
           else
            activeContractsFx_EUR{end+1} = contractsFx{k,1};    
           end 
            temp=0;
            end
            tmp=0;
        end
        if long<=LONG_DIST_FX && not(isempty(instr_long))&& not(isempty(valuationNumber))
            tmp=randi(length(instr_long));
            for j=1:tmp
            temp=randi(length(instr_long));
            k=k+1;
            instr_long{temp}.short=round(rand(1)); %L�ng eller kort position slumpas.
            contractsFx{k,1} = instr_long{temp};
           if instr_long{temp}.foreignCurrency == 'SEK'
            activeContractsFx_SEK{end+1} = contractsFx{k,1};
           else
            activeContractsFx_EUR{end+1} = contractsFx{k,1};    
           end 
            temp=0;
            end
            tmp=0;
        end
        end
       n=0;
end
      if(not(isempty(valuationNumber)))
        activeContractsFx_SEK= getActiveContract(activeContractsFx_SEK,i);
        priceOfFx_SEK = priceFxForward(activeContractsFx_SEK,valuationNumber,businessDayList,forwardMatrixUSD, forwardMatrixSEK, curSEKUSD, i);
        sumOfPriceFx_SEK(i-startDate+1,1) = sum(priceOfFx_SEK);      
        activeContractsFx_EUR= getActiveContract(activeContractsFx_EUR,i);
        priceOfFx_EUR = priceFxForward(activeContractsFx_EUR,valuationNumber,businessDayList,forwardMatrixUSD, forwardMatrixEUR, curEURUSD, i);
        sumOfPriceFx_EUR(i-startDate+1,1) = sum(priceOfFx_EUR);
      end
end

sumOfPortfolio = sumOfPriceFx_SEK+sumOfPriceFx_EUR;
PortfolioFXW=Portfolio(contractsFx,sumOfPortfolio,datenum(1999,10,25),datenum(2014,10,30));       
