function [ eigenVector, eigenValue, explanation] = determineRiskFactors(spotRateMatrix)
%determineRiskFactors - determine risk factors trough a forward matrix

%Input: 
%forwardMatrix - Matrix containing forward rates

%Output:   
%eigenValue - size of risk factor
%eigenVector - the vector containing risk factors
%explanation - Explanation of the covariance matrix for each risk factor in percentage  

% Author: Johan Hyd�n
% November 2014; Last revision: 10-11-2014

%[eigenVector, eigenValue, explanation] = pcacov(cov(forwardMatrix));
[eigenVector, eigenValue, explanation] = pcacov(cov(spotRateMatrix));