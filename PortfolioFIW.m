% Portfolio containing FRA and IRS. Updated every two week since 1999
Fra=usdFraDataBase(datenum(1999,10,25));
Irs=usdIrsDataBase(datenum(1999,10,25));
contractsFra={Fra{15};Fra{20}};
activeContractsFra = contractsFra;
contractsIrs={Irs{1};Irs{6}};
activeContractsIrs = contractsIrs;
startDate = datenum(1999,10,25);
endDate = datenum(2000,10,25);
SHORT_DIST_FRAIRS=0.36;
MID_DIST_FRAIRS=0.42;
LONG_DIST_FRAIRS=0.27; 
k=2;
l=2;
n=-1;
sumOfPriceFra = zeros(endDate-startDate+1,1);
sumOfPriceIrs = zeros(endDate-startDate+1,1);
sumOfPortfolio = zeros(endDate-startDate+1,1);


    for i=startDate:endDate
      valuationNumber = find(businessDayList == i);
      n=n+1;
      if n==14
     %------------------------------FRA------------------------------         
       if isContractsIssued(i,usdFraDataBase)
        [instr_short,instr_mid,instr_long]=splitContractByLength(usdFraDataBase(i));%ska �ndras

        if not(isempty(instr_short))&& not(isempty(valuationNumber))
            n=length(instr_short);
            tmp=randi(n);
            for j=1:tmp
            m=length(instr_short);
            temp=randi(m);
            k=k+1;
            instr_short{temp}.short=round(rand(1)); %L�ng eller kort position slumpas.
            contractsFra{k,1}= instr_short{temp};
            activeContractsFra{end+1} = contractsFra{k,1};
            temp=0;
            end
        tmp=0;
        end
      end

     
      %-------------------------------IRS------------------------------
          if isContractsIssued(i,usdIrsDataBase)
    [short,mid,long]=getPortfolioDistribution(contractsIrs,i);
    [instr_short,instr_mid,instr_long]=splitContractByLength(usdIrsDataBase(i)); %ska �ndras
    
    if short<=SHORT_DIST_FRAIRS && not(isempty(instr_short))&& not(isempty(valuationNumber))
        tmp=randi(length(instr_short));
        for j=1:tmp
            temp=randi(length(instr_short));
            l=l+1;
            instr_short{temp}.short=round(rand(1)); %L�ng eller kort position slumpas.
            contractsIrs{l,1} = instr_short{temp};
            activeContractsIrs{end+1} = contractsIrs{l,1};
            temp=0;
        end
        tmp=0;
    end
    if mid<=MID_DIST_FRAIRS && not(isempty(instr_mid))&& not(isempty(valuationNumber))
        tmp=randi(length(instr_mid));
        for j=1:tmp
            temp=randi(length(instr_mid));
             l=l+1;
            instr_mid{temp}.short=round(rand(1)); %L�ng eller kort position slumpas.
            contractsIrs{l,1} = instr_mid{temp};
            activeContractsIrs{end+1} = contractsIrs{l,1};
            temp=0;
           
        end
        tmp=0;
    end
    if long<=LONG_DIST_FRAIRS && not(isempty(instr_long))&& not(isempty(valuationNumber))
        tmp=randi(length(instr_long));
        for j=1:tmp
            temp=randi(length(instr_long));
            l=l+1;
            instr_long{temp}.short=round(rand(1));%L�ng eller kort position slumpas.
            contractsIrs{l,1} = instr_long{temp};
            activeContractsIrs{end+1} = contractsIrs{l,1};
            temp=0;
        end
        tmp=0;
    end
   end
     n=0;
      end
      if(not(isempty(valuationNumber)))
        activeContractsIrs= getActiveContract(activeContractsIrs,i);
        priceOfIrs = priceIrs(activeContractsIrs, LIBOR3M, valuationNumber, forwardMatrixUSD, i);
        sumOfPriceIrs(i-startDate+1,1) = sum(priceOfIrs);
        
        activeContractsFra = getActiveContract(activeContractsFra,i);
        priceOfFra = priceFra(activeContractsFra, valuationNumber,forwardMatrixUSD,i);
        sumOfPriceFra(i-startDate+1,1)=sum(priceOfFra);
        
    end
 end

    sumOfPortfolio = sumOfPriceIrs + sumOfPriceFra;
    contractsArray=[contractsFra;contractsIrs];
    PortfolioFRAIRSW=Portfolio(contractsArray,sumOfPortfolio,datenum(1999,10,25),datenum(2014,10,30));