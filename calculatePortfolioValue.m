function valueOfPortfolio = calculatePortfolioValue(portfolioArray,lambda)%(g, h, lambda, H)

numberOfRiskFactors = length(lambda);
%Plocka ut H, g och h

%create vech(lambda'lambda)
vechLambda = createVech(lambda'*lambda);

%create H bar
D = createMatrixD(numberOfRiskFactors):
numberOfContracts = length(portfolioArray);

matrixSizeColoumn = numberOfContracts;
matrixSizeRow = numberOfRiskFactors*(numberOfRiskFactors+1)/2;

hBarVechMatrix = zeros(matrixSizeRow, matrixSizeColoumn);
hBar = zeros(matrixSizeRow, matrixSizeColoumn);

for i = 1:numberOfContracts
    hBarVechMatrix(:,i) = createVech(H(i));
end    

hBar = D*hBarVechMatrix;

%Calculate G

for j = 1:numberOfContracts
    G(:,j) = hBarVechMatrix(:,i) = createVech(H(i));
end   


%Calculate value of portfolio

valueOfPortfolio = lambda'*G*h+1/2*vechLambda'*hBar*h;

end

function vech = createVech(symmetricMatrix)
    matrixDimension = length(symmetricMatrix);

    startOfVector = 1;
    sizeOfVector = matrixDimension;

    vech = zeros(sum([1:matrixDimension]),1);

    for i=1:matrixDimension
        vech(startOfVector:sizeOfVector+startOfVector-1)=symmetricMatrix(i:end,i);

        startOfVector = startOfVector+sizeOfVector;
        sizeOfVector=sizeOfVector-1;
    end
    
end

function D = createMatrixD(numberOfRiskFactors)

sizeOfDMatrix = numberOfRiskFactors*(numberOfRiskFactors+1)/2;
vectorD = zeros(1,sizeOfDMatrix)
for i=1:sizeOfDMatrix
    equationTrue = 0;
    for j = 1:numberOfRiskFactors
        if (i == j*(2*numberOfRiskFactors+3-j)/2-numberOfRiskFactors)
            equationTrue = 1;
        end
    end
    if (equationTrue == 1)
        vectorD(i) = 1;
    else
        vectorD(i) = 2;
    end
end

D = diag(vectorD)
   
end