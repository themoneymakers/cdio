function valueZeroCouponBonds = valuateZeroCouponBonds(valuationNumber, discountMatrix, valuationDate, time)
    if(valuationNumber-100 < 1);
        error('Not enough discount factors to do this calculation')
    end
    
    valuationNumbers = [valuationNumber - 100 : valuationNumber];
    discountRates = getDiscountRates(valuationNumbers, valuationDate, discountMatrix, time);
    
    valueZeroCouponBonds = exp(-discountRates);
    valueZeroCouponBonds = flipud(valueZeroCouponBonds);
end

function discountRates = getDiscountRates(valuationNumbers, valuationDate, discountMatrix, time)
% Finds discount rate from the discountMatrix
%
% Inputs   
%   valuationNumber - the number of the row corresponding to the date that
%   is valued
%   discountMatrix
%   valuationDate
%   issueDate
%   maturityDate
%   terminationDate
%
% Outputs
%   discountRate - the two discount factors needed for calculating the FRA
%
%------------- BEGIN CODE --------------
numberOfValuations = length(valuationNumbers);
discountRates = zeros(numberOfValuations,1);

coloumn = ceil(time*365);

for i = 1:numberOfValuations
    
    discountRates(i) = discountMatrix(valuationNumbers(i),coloumn);
end
end

%------------- END CODE --------------

