function dateMap = importExcelFile(fileName, sheetName,businessVector)
% importExcelFile - The file creates Instrument object from a sheet in Excel file
%
% Inputs:
%   fileName - Name of Excel file
%   sheetName - Name of sheet in Excel File
%
% Outputs:
%   dateMap - An containers.Map with dates (yyyymmdd) as keys to a
%             containers.map with ricCode as keys to InterestRateInstrument
%             objects
%
% Examples: 
% >> importExcelFile('FRA_IRS.xlsx','FRA USD - ASK')
% 
% ans = 
% 
%   containers.Map handle
%   Package: containers
% 
%   Properties:
%         Count: 22
%       KeyType: 'char'
%     rateType: 'any'
% 
%   Methods, Events, Superclasses
%
%
% Other m-files required: Instrument.m
% MAT-files required: none
% Other files required: none
%
% Author: Emil Karlsson
% Oktober 2014; Last revision: 25-Okt-2014

%------------- BEGIN CODE --------------

  tic
  %import Excel file
  [numerical, text] = xlsread(fileName, sheetName);
  toc
  tic
  dateMap = containers.Map('keyType','double','ValueType','any'); %containers.Map of all created Instrument classes

  numberOfInstrument = (size(numerical,2)+1)/2; %Number of instruments
  
  
  
  %Loop and create all instruments
  for i = 1 : numberOfInstrument
    rateList = numerical(:,2*i-1);
    rateList = rateList(isfinite(rateList(:, 1)), :); %Removes all NaN
    rateListLength = size(rateList,1); %Length of rates
    
    ricCodeText = text(2,2*i); %Cell array
    ricCodeString = ricCodeText{1}; %Convert to string
    
    %Parses info from RicCode
    isFra = isstrprop(ricCodeString(4),'digit');
    
    dateList = text(4:end,2*i);
    timeStamp = datenum(dateList(1:rateListLength,1),'yyyy-mm-dd'); %Convert to datenum and removes unwanted entries

    rateMatrix = [rateList timeStamp];
    numberOfRates = size(rateMatrix,1);
    
    for dateIterator = 1 : numberOfRates
        % No allocation for arrays, but just takes ~sec longer than before
        % so will not bother with it
        if not(isKey(dateMap, rateMatrix(dateIterator,2)))
            if isFra
                dateMap(rateMatrix(dateIterator,2)) ={ForwardRateAgreement(ricCodeString, rateMatrix(dateIterator,2), rateMatrix(dateIterator,1),businessVector)};
            else
                dateMap(rateMatrix(dateIterator,2)) ={InterestRateSwap(ricCodeString, rateMatrix(dateIterator,2), rateMatrix(dateIterator,1),businessVector)};
            end
        else
            if isFra
                dateMap(rateMatrix(dateIterator,2)) = [dateMap(rateMatrix(dateIterator,2));{ForwardRateAgreement(ricCodeString, rateMatrix(dateIterator,2), rateMatrix(dateIterator,1),businessVector)}];
            else
                dateMap(rateMatrix(dateIterator,2)) = [dateMap(rateMatrix(dateIterator,2));{InterestRateSwap(ricCodeString, rateMatrix(dateIterator,2), rateMatrix(dateIterator,1),businessVector)}];
            end
        end
        
      disp(dateIterator)
    end
 
   
  end
  toc %Checks computation time
  
  %  For all dates, sort the array w r t maturityDate
  disp('Now sorting the arrays')
  tic
  containerKeys = keys(dateMap);
  for keyIterator=1:length(containerKeys)
      dateMap(containerKeys{keyIterator}) = instrumentSort(dateMap(containerKeys{keyIterator}),'maturityDate');
  end
  toc

end