classdef InterestRateInstrument
    %INSTRUMENT Base class for the financial instruments
    %   Base class, might be useful because there is only small difference
    %   between the different instruments. All instruments inheirits
    %   fundamental properties and methods from this class.
    
    properties
        % member data
        ricCode; % RIC-code of bese contract
        issueDate; % Date of first data
        nominalValue;
        short;

    end
    
    methods
        function INS = InterestRateInstrument(r,i) % constructor
            INS.ricCode = r;
            INS.issueDate = i;
            INS.nominalValue = 100;
            INS.short = false;
        end
    end
end

%currency = ricCodeString(1:3); %First three letters of ricCode is the currency