function price = evaluateGeneralCasePortfolio(portfolio) %Values from risk factors as input
% functionName - One line description of what the function performs (H1 line)
% Optional file header info (to give more details about the function than in the H1 line)
% Optional file header info (to give more details about the function than in the H1 line)
% Optional file header info (to give more details about the function than in the H1 line)
%
% Inputs:
%    input1 - Description
%    input2 - Description
%    input3 - Description
%
% Outputs:
%    output1 - Description
%    output2 - Description
%
% Examples: 
% >> add3(7)
%
% ans =
%
% 10 
%
% >> add3([2 4])
%
% ans =
%
% 5 7 
%
% Other m-files required: none
% MAT-files required: none
% Other files required: none
%
% Author: FirstName FamilyName
% September 2014; Last revision: 14-Sep-2014

%------------- BEGIN CODE --------------

%Assumptions on risk factors, estimated from data
mu = zeros(5,1);
Sigma = eye(5);

%Calculated from day to day change in the risk factors
randomVector = mvnrnd(mu, Sigma)';

%Price for a single asset, based on change in risk factors
price = priceScalar + gradVector'*randomVector + 1/2*(randomVector'*hessianMatrix*randomVector);

%------------- END OF CODE --------------

