%pastDates = xlsread('priser','F2:F35');
%pastPrices = xlsread('priser','B2:E100');
%pastReturns = calculatePastReturns(pastPrices);
%pastVolatility = calculateVolatility(pastReturns);

valuationDate(1) = 732836;
valuationDate(2) = 733017;

valuationNumber(1) = find(businessDayList==valuationDate(1));
valuationNumber(2) = find(businessDayList==valuationDate(2));

 priceBondThreeMonths = valuateZeroCouponBonds(valuationNumber,discountMatrix,valuationDate(1),3/12);
 priceBondSixMonths = valuateZeroCouponBonds(valuationNumber,discountMatrix,valuationDate(1),6/12);
 priceBondTwoYear = valuateZeroCouponBonds(valuationNumber,discountMatrix,valuationDate(1),1);
% 
 pastReturnsThreeMonths = calculatePastReturns(priceBondThreeMonths)
 pastReturnsSixMonths = calculatePastReturns(priceBondSixMonths);
% pastReturnsTwoYear = calculatePastReturns(priceBondTwoYear);
% 
 pastVolatilityThreeMonths = calculateVolatility(pastReturnsThreeMonths);
 pastVolatilitySixMonths = calculateVolatility(pastReturnsSixMonths);
% pastVolatilityTwoYear = calculateVolatility(pastReturnsTwoYear);
% 

% tempIrs = getInstrumentArray(getDateMap(732836,usdIrsDataBase))
% tempIrs = tempIrs(1)
% valuationDate = 733017;
% valuationNumber = 4110;


%tempFra = getInstrumentArray(getDateMap(732836,usdFraDataBase))