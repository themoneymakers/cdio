function [activeContract] = getActiveContract(portfolioArray, date)
activeContract={};
for i = 1:length(portfolioArray)
    if (portfolioArray{i}.maturityDate-date > 0)
        activeContract{end+1,1}=portfolioArray{i};
    end
end
end