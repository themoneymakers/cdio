function correlation = calculateCorrelation(pastReturnI, pastReturnJ)
% calculateCorrelationMatrix - Calculates past covariance
% Uses past returns of two assets to calculate covariance
%
% Inputs:
%    pastReturnI [matrix] - past returns of instruments over a time period.
%    pastReturnI [matrix] - past returns of instruments over a time period.
%
% Outputs:
%    correlation [matrix] - The correlation between instrument I and J.
%
% Help functions:
%    calculatePastCovariance
%
% Other m-files required: calculatePastReturns
% MAT-files required: none
% Other files required: none
%
% Author: Alexandra Stroh
% November 2014; Last revision: 12-11-2014

%------------- BEGIN CODE --------------

% --------------------------------
% Calculate Volatility
pastVolatilityTemp = calculateVolatility([pastReturnI pastReturnJ]);
pastVolatility = pastVolatilityTemp(1,:);

% ---------------------------------  
% Calculate covariance
pastCovarianceTemp = calculatePastCovariance(pastReturnI,pastReturnJ);
pastCovariance = pastCovarianceTemp(1);

% ---------------------------------  
% Calculate correlation

correlation = pastCovariance/(pastVolatility(1)*pastVolatility(2));

end

%------------- END OF CODE --------------

%%------------ HELP FUNCTIONs -----------------------------------------


function pastCovariance = calculatePastCovariance(pastReturnsI, pastReturnsJ)
% calculatePastCovariance - Calculates past covariance
% Uses past returns of two assets to calculate covariance
%
% Inputs:
%    pastReturnsI [vector] - past returns of an instrument over a time period.
%    Each row is a time (from newest to oldest)
%    pastReturnsJ [vector] - past returns of an instrument over a time period.
%    Each row is a time (from newest to oldest)
%
% Outputs:
%    pastCovariance [vector] - The past covariance at each time
%
% Help functions:
%    calculatePastCovariance
%

%------------- BEGIN CODE --------------

LAMBDA = 0.94;
numberOfData = length(pastReturnsI);
pastCovariance = zeros(numberOfData,1);

% --------------------------------
% Calculate the starting value
meanPastReturnI = mean(pastReturnsI);
meanPastReturnJ = mean(pastReturnsJ);

for j = 1:numberOfData
    sumOfReturns(j) = (pastReturnsI(j) - meanPastReturnI)*(pastReturnsJ(j)-meanPastReturnJ);
end

pastCovariance(numberOfData) = 1/(numberOfData-1)*sum(sumOfReturns);

%----------------------------------
% Calculate past covariance with EWMA

for i = numberOfData-1:-1:1
    pastCovariance(i) = LAMBDA*pastCovariance(i+1)+(1-LAMBDA)*pastReturnsI(i+1)*pastReturnsJ(i+1);
end

%-------------------------------
% Converting from daily to yearly

pastCovariance = pastCovariance*252;

end

%------------- END OF CODE --------------
