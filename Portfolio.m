classdef Portfolio
    %INSTRUMENT Base class for portfolios
    
    properties
        % member data
        instrumentArray; %Cell array of instrument objects
        priceVector; %Vector containing the portfoliovalue each work day
        startDate; %start date for the portfolio as datenum
        endDate; %end date as datenum
    end
    
    methods
        function portfolio = Portfolio(iA, pV, sD, eD)
            portfolio.instrumentArray = iA;
            portfolio.priceVector = pV;
            portfolio.startDate = sD;
            portfolio.endDate = eD;
        end
    end
end