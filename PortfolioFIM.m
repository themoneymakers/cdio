% Portfolio containing FRA and IRS. Updated every two week since 1995
contractsFra={};
contractsIrs={};
startDate = datenum(1995,10,16);
endDate = datenum(2014,10,16);
SHORT_DIST_FX=0.36;
MID_DIST_FX=0.42;
LONG_DIST_FX=0.27; 
n=-1;

    for i=startDate:endDate
        n=n+1;
          if n==14
     %------------------------------FRA------------------------------         
          if isContractsIssued(i,usdFraDataBase)
        [short,mid,long]=getPortfolioDistribution(contractsFra,i);
        [instr_short,instr_mid,instr_long]=splitContractByLength(getInstrumentArray(usdFraDataBase(i)));%ska �ndras

        if short<=SHORT_DIST_FRAIRS && not(isempty(instr_short))
            tmp=Randi(length(instr_short));
            for j=1:tmp
                temp=Randi(length(instr_short));
                contractsFra = {contractsFra, instr_short(temp)};
                temp=0;
            end
            tmp=0;
        end
        if mid<=MID_DIST_FRAIRS && not(isempty(instr_mid))
            tmp=Randi(length(instr_mid));
            for j=1:tmp
                temp=Randi(length(instr_mid));
                contractsFra = {contractsFra, instr_mid(temp)};
                temp=0;
            end
            tmp=0;
        end
        if long<=LONG_DIST_FRAIRS && not(isempty(instr_long)) 
            tmp=Randi(length(instr_long));
            for j=1:tmp
                temp=Randi(length(instr_long));
                contractsFra = {contractsFra, instr_long(temp)};
                temp=0;
            end
            tmp=0;
        end
          end
      %-------------------------------IRS------------------------------
      if isContractsIssued(i,usdIrsDataBase)
        [short,mid,long]=getPortfolioDistribution(contractsIrs,i);
        [instr_short,instr_mid,instr_long]=splitContractByLength(getInstrumentArray(usdIrsDataBase(i))); %ska �ndras

        if short<=SHORT_DIST_FRAIRS && not(isempty(instr_short))
            tmp=Randi(length(instr_short));
            for j=1:tmp
                temp=Randi(length(instr_short));
                instr_short(temp)
                contractsIrs = {contractsIrs, instr_short(temp)};
                temp=0;
            end
            tmp=0;
        end
        if mid<=MID_DIST_FRAIRS && not(isempty(instr_mid))
            tmp=Randi(length(instr_mid));
            for j=1:tmp
                temp=Randi(length(instr_mid));
                contractsIrs = {contractsIrs, instr_mid(temp)};
                temp=0;
            end
            tmp=0;
        end
        if long<=LONG_DIST_FRAIRS && not(isempty(instr_long))
            tmp=Randi(length(instr_long));
            for j=1:tmp
                temp=Randi(length(instr_long));
                contractsIrs = {contractsIrs, instr_long(temp)};
                temp=0;
            end
            tmp=0;
        end
        end
           n=0;
        end
    end
    
    PortfolioFRAIRSW=Portfolio(contractsArray,datenum(1995,10,16),datenum(2014,10,16));