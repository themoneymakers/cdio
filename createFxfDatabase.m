% Script for creating a database for FX-future

% Which currencies to use? 
% Probably SEK, EUR (andra st�ds ej av j�rgen)
% Which maturities to use?
% Probably 3m, 6m, 9m, 12m
% 
% How store? 
% a) (date:(maturity:(currency)))
% b) (date:(currency:(maturity)))
% c) (date: (currency, maturity)) (K�r den h�r men sortera p� l�ptid)

% L�ptider: 1m, 3m, 6m, 9m, 1y, 2y, 3y, 4y, 5y, 6y, 8y, 10y

%pseudokod

maturities = [1;3;6;9;12;24;36;48;60;72;96;120];
numberOfDifferentCurrencies = 2; % SEK,EUR
arrayLength = numberOfDifferentCurrencies*length(maturities);


%skapa containers.Map
usdFxfDataBase = containers.Map('KeyType', 'double', 'ValueType', 'any');
length(businessDayList)
for dateIterator = 1:length(businessDayList)
    disp(dateIterator)
    %skapa containers.Map
    issueDate = businessDayList(dateIterator);
    instrumentArray = cell(arrayLength,1);
    for instrumentIterator = 1:length(maturities)
        %skapa rickod f�r eurotermin
        tempRic= strcat('EURUSD',num2str(maturities(instrumentIterator)),'M');
        %skapa eurotermin p� plats 2*instrumentIterator-1 i instrumentArray
        instrumentArray{2*instrumentIterator-1} = ForeignExchangeForward(tempRic,businessDayList(dateIterator),businessDayList);
        %skapa rickod f�r sektermin
        tempRic= strcat('SEKUSD',num2str(maturities(instrumentIterator)),'M');
        %skapa kronortermin p� plats 2*instrumentIterator+1 i instrumentArray
        instrumentArray{2*instrumentIterator} = ForeignExchangeForward(tempRic,businessDayList(dateIterator),businessDayList);
    end
    %L�gg in arrayen i mappen med nyckel businessDayList(dateIterator)
    usdFxfDataBase(businessDayList(dateIterator)) = instrumentArray;
end