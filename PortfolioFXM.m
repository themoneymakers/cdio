% Portfolio containing FX forward. Updated every two weeks since 1995
contracts={};
startDate = datenum(1995,10,16);
endDate = datenum(2014,10,16);
SHORT_DIST_FX=0.77;
MID_DIST_FX=0.20;
LONG_DIST_FX=0.084;
n=-1;


for i=startDate:endDate
    n=n+1;
      if n==14
        if isContractsIssued(i,usdFxDatabase)
        [short,mid,long]=getPortfolioDistribution(contracts,i);
        [instr_short,instr_mid,instr_long]=splitContractByLength(getInstrumentArray(usdFxDataBase(i))); %ska �ndras
         if short<=SHORT_DIST_FX && not(isempty(instr_short))
            tmp=Randi(length(instr_short));
            for j=1:tmp
                temp=Randi(length(instr_short));
                instr_short(temp);
                contracts = {contracts, instr_short(temp)};
                temp=0;
            end
            tmp=0;
         end
        if mid<=MID_DIST_FX && not(isempty(instr_mid))
            tmp=Randi(length(instr_mid));
            for j=1:tmp
                temp=Randi(length(instr_mid));
                contracts = {contracts, instr_mid(temp)};
                temp=0;
            end
            tmp=0;
        end
        if long<=LONG_DIST_FX && not(isempty(instr_long))
            tmp=Randi(length(instr_long));
            for j=1:tmp
                temp=Randi(length(instr_long));
                contracts = {contracts, instr_long(temp)};
                temp=0;
            end
            tmp=0;
        end
        end
       n=0;
    end
end
    
PortfolioFXW=Portfolio(contractsArray,datenum(1995,10,16),datenum(2014,10,16));
        

%testFra = Portfolio1.instrumentAPorray{2} plocka ut andra instrumentet